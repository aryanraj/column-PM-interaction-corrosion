%{
  Considering confining pressure
  Taken from SAATCIOGLU AND RAZVI MODEL
  Link for reference: http://www2.ce.metu.edu.tr/~ce581/docs/Lecture%20Notes/Chapter%201/1_3_MATERIAL_BEHAVIOR.pdf

  Parameters:
    strain: "Concrete Strain"
    concreteStrength: "Concrete Strength"
    tieData:
      1: "Tie Yield Strength"
      2: "Tie Area"
      3: "Tie Spacing"
    coreData:
      1: "Is Circle"
        % If "Is Circle" = 1
      2: "Core Diameter"
        % Else
      2: "Core dimension X"
      3: "Core dimension Y"
      4: "Tie unsupported length X"
      5: "Tie unsupported length Y"

  Units:
    Stress: MPa
    Length: mm

  Return:
    stress: "Concrete Stress"
%}

function [stress] = Stress_Concrete_from_Strain(strain, concreteStrength, tieData, coreData)
  if strain < 0
    stress = 0;
    return
  end
  tieYieldStrength = tieData(1);
  tieArea = tieData(2);
  tieSpacing = tieData(3);
  shape = coreData(1);
  if shape == 1
    coreDiameter = coreData(2);
    sigma2e = (2*tieArea*tieYieldStrength)/(coreDiameter*tieSpacing);
  else
    s = tieSpacing;
    bkx = coreData(2);
    bky = coreData(3);
    ax = coreData(4);
    ay = coreData(5);
    sigma2x = 2*tieArea*tieYieldStrength*(sin(pi/2)+sin(pi/4))/(s*bkx);
    betax = 0.26*sqrt((bkx/ax)*(bkx/s)*(1.0/sigma2x));
    sigma2ex = betax * sigma2x;
    sigma2y = 2*tieArea*tieYieldStrength*(sin(pi/2)+sin(pi/4))/(s*bky);
    betay = 0.26*sqrt((bky/ay)*(bky/s)*(1.0/sigma2y));
    sigma2ey = betay * sigma2y;
    sigma2e = (sigma2ex*bkx + sigma2ey*bky)/(bkx + bky);
  end
  k3 = 0.85;
  k1 = 6.7/(sigma2e^0.17);
  fcc = k3*concreteStrength + k1*sigma2e;

  epsilonco = 0.002;
  lambda = (k1*sigma2e) / (k3*concreteStrength);
  epsiloncoc = epsilonco * (1 + 5*lambda);
  stress = fcc * ((2*strain/epsiloncoc) - (strain/epsiloncoc)^2)^(1/(1 + 2*lambda));
  stress = min([stress, fcc]);
end
