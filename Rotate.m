function [points] = Rotate(points, rad)
  rotator = [
     cos(rad)  sin(rad)
    -sin(rad)  cos(rad)
  ];
  points = points * rotator;
end
