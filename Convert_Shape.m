function [points] = Convert_Shape(topCurve, bottomCurve, xStart, xEnd, sliceSize)
  points = [];
  for i=(xStart+sliceSize/2):sliceSize:xEnd
    x = i;
    yEnd = topCurve(x);
    yStart = bottomCurve(x);
    for j=(yStart+sliceSize/2):sliceSize:yEnd
      y = j;
      points = [points;x y];
    end
  end
end
