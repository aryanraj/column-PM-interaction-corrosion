%{
  Naming Scheme:
    CONSTANT_NAMES
    Function_Names
    variableNames

  Notes:
    Global Variables are CONSTANTS
%}

% Concrete Core Data
% NOTE: unable to make it automatic
coreData = zeros(5,1);
if strcmp(SHAPE_TYPE, 'circle')
  coreData(1) = true;
  coreData(2) = CORE_DIAMETER;
else
  coreData(1) = false;
  coreData(2) = CORE_DIMENSION_X;
  coreData(3) = CORE_DIMENSION_Y;
  coreData(4) = TIE_UNSUPPORTED_LENGTH_X;
  coreData(5) = TIE_UNSUPPORTED_LENGTH_Y;
end

if WITH_CONFINING_LOAD == false
  Stress_Concrete_from_Strain = @(strain, stressMax) max(stressMax*(2*(min(strain, 0.002)/0.002)-(min(strain, 0.002)/0.002)^2), 0);
end
Stress_Steel_from_Strain = @(strain, stressStrainData) interp1(stressStrainData(:,1), stressStrainData(:,2), strain);
Steel_Buckling_Stress = @(diameter,effectiveLength) (pi^2 * STEEL_ELASTIC_MODULUS * pi/64*diameter^4)/(effectiveLength^2 * pi/4*diameter^2);


strainProfile = [];
for y=0.0019:-0.0001:0.000
  xu = 3/7*xLength + ((4/7)*xLength/(0.002-y))*0.002;
  endStrainByXu = 0.002/(xu-3/7*xLength);
  strainProfile = [strainProfile; xu endStrainByXu];
end
for x=xLength:-10:10
  xu = x;
  endStrainByXu = 0.0035/xu;
  strainProfile = [strainProfile; xu endStrainByXu];
end
% strainProfile = [1.2*xLength 0.002/(1.2*xLength - 3/7*xLength)];

steelDiameter = STEEL_CONFIGURATION(:,1);
steelDistance = STEEL_CONFIGURATION(:,2);
steelArea = pi/4*steelDiameter.^2;
steelCorrosionAmount = 0.046*CORROSION_RATE*TIME./(steelDiameter)*100.*STEEL_CONFIGURATION(:,4);
% Reduction in area due to corrosion
steelArea = steelArea .* (1 - 0.01*steelCorrosionAmount);
steelDiameter = sqrt(steelArea*4/pi);

% Calculations for tie data
tieCorrosionAmount = 0.046*CORROSION_RATE*TIME./(TIE_DIAMETER)*100;
tieData = zeros(3,1);
tieData(1) = Stress_Steel_from_Strain(0.5, STEEL_STRESS_STRAIN_DATA) * (1 - 0.005*tieCorrosionAmount);
tieData(2) = pi/4*TIE_DIAMETER^2 * (1 - 0.01*tieCorrosionAmount);
tieData(3) = TIE_SPACING;
% end

nSteelBars = size(STEEL_CONFIGURATION,1);
result = [];
for i = 1:size(strainProfile,1)
  ele = strainProfile(i,:);
  xu = ele(1);
  endStrainByXu = ele(2);
  forceDistance = [];
  steelStress = [zeros(nSteelBars,1)];
  % Calculations for Stresses in steel
  for i=1:nSteelBars
    if steelArea <= 0
      continue
    end
    x = steelDistance(i);
    strain = (xu-x)*endStrainByXu;

    % Reduction in strain due to corrosion
    strain = strain*(1-0.05*min(steelCorrosionAmount(i),20));

    % Calculation of stress in steel from strain
    stress = Stress_Steel_from_Strain(strain, STEEL_STRESS_STRAIN_DATA);

    % Reduction in Stress value due to corrosion
    stress = stress*(1-0.005*steelCorrosionAmount(i));

    % Calculate Buckling Stress
    effectiveLength = 0.85*TIE_SPACING;
    bucklingStress = max(Steel_Buckling_Stress(steelDiameter(i), effectiveLength), 0);
    stress = min([stress bucklingStress]);

    % Effective stress = steel - concrete
    stress = stress - Stress_Concrete_from_Strain(strain, 0.477*CONCRETE_STRESS_MAX, tieData, coreData);

    area = steelArea(i);
    force = stress*area;
    endDistance = x;
    forceDistance = [forceDistance; force endDistance];
  end
  % Calculations for Stresses in concrete
  for i=1:size(concretePoints,1)
    x = concretePoints(i,1);
    strain = (xu-x)*endStrainByXu;
    stress = Stress_Concrete_from_Strain(strain, 0.477*CONCRETE_STRESS_MAX, tieData, coreData);
    area = concretePointArea;
    force = stress*area;
    endDistance = x;
    forceDistance = [forceDistance; force endDistance];
  end
  totalForce = sum(forceDistance(:,1));
  totalMoment = sum(forceDistance(:,1) .* (forceDistance(:,2) - shapeCentroid(1)));
  result = [result; totalForce totalMoment xu];
end

% axis([0, max(-result(:,2))*1.2, min(result(:,1))*1.2, max(result(:,1))*1.2]);
% p = plot(-result(:,2), result(:,1), PLOT_STYLE);
