concretePointArea = SLICE_SIZE^2;

concretePoints = Convert_Shape(TOP_CURVE, BOTTOM_CURVE, X_START, X_END, SLICE_SIZE);

concretePoints = Rotate(concretePoints, ROTATE_ANGLE);
STEEL_CONFIGURATION(:,2:3) = Rotate(STEEL_CONFIGURATION(:,2:3), ROTATE_ANGLE);

xStart = min(concretePoints(:,1)) - SLICE_SIZE/2;
xEnd = max(concretePoints(:,1)) + SLICE_SIZE/2;
xLength = xEnd - xStart;
concretePoints(:,1) = concretePoints(:,1) - xStart;
STEEL_CONFIGURATION(:,2) = STEEL_CONFIGURATION(:,2) - xStart;

shapeCentroid = Get_Centroid(concretePoints);
