%This is the configuration file.
% clear

% Shape of the section.
TOP_CURVE = @(x) 300;
BOTTOM_CURVE = @(x) 0;
X_START = 0;
X_END = 500;
SLICE_SIZE = 10;
ROTATE_ANGLE = pi;

% Steel Configuration
STEEL_CONFIGURATION = [
  25   60.5   60.5  0
  25   60.5  239.5  1
  25  250     60.5  0
  25  250    230.5  1
  25  439.5   60.5  0
  25  439.5  230.5  1
]; % [d(mm) x(mm) y(mm) corrosionFactor]
STEEL_ELASTIC_MODULUS = 2e5;  % MPa
STEEL_STRESS_STRAIN_DATA = xlsread('steel_stress.xlsx','415');

% COncrete Configuration
CONCRETE_STRESS_MAX = 25;   % MPa
WITH_CONFINING_LOAD = false;

% Tie specifications
TIE_SPACING = 600;      % mm
TIE_DIAMETER = 8;       % mm

SHAPE_TYPE = 'rectangle';
CORE_DIMENSION_X = 35;  % mm
CORE_DIMENSION_Y = 35;  % mm
TIE_UNSUPPORTED_LENGTH_X = 430; % mm
TIE_UNSUPPORTED_LENGTH_Y = 230; % mm
% SHAPE_TYPE = 'circle';
% CORE_DIAMETER = 430;  % mm

% For corrosion
TIME = 50;               % year
CORROSION_RATE = 1;     % microAmpere/cm

% Plotting
PLOT_STYLE = '..';
