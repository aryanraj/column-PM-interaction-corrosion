pairData = [];
for i=0:pi/24:pi
  take_input_rectangle
  ROTATE_ANGLE = i;
  process_shape
  main
  pairData = [pairData; -result(:,2)*cos(ROTATE_ANGLE) -result(:,2)*sin(ROTATE_ANGLE) result(:,1)];
  title(ROTATE_ANGLE);
end
