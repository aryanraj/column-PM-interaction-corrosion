%This is the configuration file.
% clear

% Shape of the section.
RADIUS = 250;
STEEL_INNER_OFFSET = 50;
TOP_CURVE = @(x) sqrt(RADIUS^2 - x^2);
BOTTOM_CURVE = @(x) -sqrt(RADIUS^2 - x^2);
X_START = -RADIUS;
X_END = RADIUS;
SLICE_SIZE = 10;
ROTATE_ANGLE = pi/2;

% Steel Configuration
STEEL_CONFIGURATION = [];
for ang=pi/3:pi/3:2*pi
  STEEL_CONFIGURATION = [STEEL_CONFIGURATION; 25 (RADIUS - STEEL_INNER_OFFSET)*cos(ang) (RADIUS - STEEL_INNER_OFFSET)*sin(ang) 0]; % [d(mm) x(mm) y(mm) corrosionFactor]
end
STEEL_CONFIGURATION(:,4) = 1;
STEEL_ELASTIC_MODULUS = 2e5;  % MPa
STEEL_STRESS_STRAIN_DATA = xlsread('steel_stress.xlsx','415');

% COncrete Configuration
CONCRETE_STRESS_MAX = 25;   % MPa
WITH_CONFINING_LOAD = false;

% Tie specifications
TIE_SPACING = 200;      % mm
TIE_DIAMETER = 8;       % mm

SHAPE_TYPE = 'rectangle';
CORE_DIMENSION_X = 35;  % mm
CORE_DIMENSION_Y = 35;  % mm
TIE_UNSUPPORTED_LENGTH_X = 430; % mm
TIE_UNSUPPORTED_LENGTH_Y = 230; % mm
% SHAPE_TYPE = 'circle';
% CORE_DIAMETER = 430;  % mm

% For corrosion
TIME = 50;               % year
CORROSION_RATE = 1;     % microAmpere/cm

% Plotting
PLOT_STYLE = '--';
