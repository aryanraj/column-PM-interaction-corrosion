function [point] = Get_Centroid(points)
  nPoints = size(points, 1);
  x = sum(points(:,1))/nPoints;
  y = sum(points(:,2))/nPoints;
  point = [x y];
end
